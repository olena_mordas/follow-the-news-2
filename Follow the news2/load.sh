#!/bin/bash
set -e

CLUSTER="http://localhost"
TOKEN="46e2fc4359ba774f8bbcb6915b69dec1819a469d8e657e82b94928d3db91d7a2dffb89a6dcc72fdaa781a778ce9f5aa1f635b8fd0f1b5fccc04290b2245cfeff"
PROJECT="1NemMiELRFa0l2CU2uYQlw"


# Cent OS
timestamp=$(date +"%Y-%m-%dT%H:%M:%S")

squirro_data_load -v \
	-c "$CLUSTER" \
	--project-id "$PROJECT" \
	-t "$TOKEN" \
    --source-script wells_media.py \
    --source-name 'Wells Media' \
    --bulk-index \
    --source-batch-size 200 \
    --map-title 'title' \
    --map-abstract 'description' \
    --map-body 'body' \
    --map-id 'link' \
    --map-url 'link' \
    --map-created-at 'created_at' \
    --news-incremental-sync \
    --news-history 3 \
    --news-state-file 'sync_state/wells_media_state.json' \
    --article-batch-size 200 \
    --article-async-threads 10 \
    --load-insurance-journal \
    --load-claims-journal \
    --facets-file 'facets.json' \
    --pipelets-file 'pipelets.json'
 #   --log-file "logs/wells_media_${timestamp}.log" \
