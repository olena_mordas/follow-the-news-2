"""
Load content from various Wells Media news sources into Squirro
"""

#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
#
# Team         : Michael Marinaccio / Synpulse
# Program      : wells_media.py
# Description  : Retrieves Wells Media news sources by leveraging RSS and archived content

import hashlib
import logging
import json
import sys
import os
import copy
from datetime import datetime
import requests
import grequests
from dateutil.relativedelta import relativedelta
from bs4 import BeautifulSoup

from squirro.dataloader.data_source import DataSource

log = logging.getLogger(__name__)

CURRENT_DATETIME = datetime.now()
DEFAULT_STATE = {'articles': {}}


class WellsMediaNews(DataSource):
    """
    Insurance Journal news plugin for the squirro_data_loader
    """

    def __init__(self):

        self.args = None
        self.all_articles_count = None
        self.insurance_journal_news_sections = [
            'http://www.insurancejournal.com/news/national/',
            'http://www.insurancejournal.com/news/international/'
        ]

        self.claims_journal_news_sections = [
            'http://www.claimsjournal.com/news/national/',
            'http://www.claimsjournal.com/news/international/'
        ]

        pass

    def connect(self, inc_column=None, max_inc_value=None):
        """
        Initialize class variables and validate load parameters
        """
        log.debug('Incremental Column: %r', inc_column)
        log.debug('Incremental Last Value: %r', max_inc_value)

        # Validate args for incremental loading
        if self.args.news_incremental_sync and not self.args.news_state_file:
            sys.exit('--news-state-file must be set to '
                     'use incremental loading')

        if self.args.news_incremental_sync:
            try:
                with open(self.args.news_state_file, 'r') as previous_state_file:
                    self.current_state = json.load(previous_state_file)
            except ValueError:
                sys.exit('Specified State File {} contains invalid JSON data'.format(
                    self.args.news_state_file))
            except IOError:
                log.warn('Specified State File {} does not exist.'
                         ' Will create it after running first data load'.format(
                             self.args.news_state_file))
                self.current_state = copy.deepcopy(DEFAULT_STATE)

        else:
            self.current_state = {}

        # Define scope of news loading
        self.sources = []

        if self.args.load_insurance_journal:
            self.sources.extend(self.insurance_journal_news_sections)

        if self.args.load_claims_journal:
            self.sources.extend(self.claims_journal_news_sections)

        if not self.sources:
            sys.exit('You must select at least one news sources')

    def disconnect(self):
        """Write json state file with loaded content."""

        if self.current_state and self.args.news_incremental_sync:
            log.info('Writing Updated State file')

            # Rotate out old state file if it exists
            if os.path.isfile(self.args.news_state_file):
                archived_state_file_name = '{filename}.{timestamp}.json'.format(
                    filename=self.args.news_state_file,
                    timestamp=datetime.now().strftime('%Y%m%d%H%M%S'))

                os.rename(self.args.news_state_file, archived_state_file_name)

            # Write New state file
            with open(self.args.news_state_file, 'w') as state_file:
                json.dump(self.current_state, state_file)
        else:
            log.info('Not Writing an updated state file')

    def get_news(self):
        """
        Yield batches of articles links
        :yield: list
        """
        log.info("Loading last %s months of news." % self.args.news_history)

        # Collect all article links
        all_article_links = self.get_article_links(self.args.news_history)
        self.all_articles_count = len(all_article_links)
        log.info('Found %s new articles!' % self.all_articles_count)

        article_batch = []
        for link in all_article_links:
            article_batch.append(link)
            if len(article_batch) >= self.args.article_batch_size:
                yield article_batch
                article_batch = []

        if article_batch:
            yield article_batch

    def get_article_links(self, history):
        """
        Collect links to all articles within defined time frame
        :return: list
        """
        article_links = []

        for url in self.sources:

            log.info('Connecting to {}'.format(url))

            for month in range(history, -1, -1):
                article_date = CURRENT_DATETIME - relativedelta(months=month)

                if len(str(article_date.month)) == 1:
                    article_month = '0' + str(article_date.month)
                else:
                    article_month = str(article_date.month)

                article_year = article_date.year

                log.info('Gathering articles from {month}-{year}'.format(
                    month=article_month,
                    year=article_year))

                archive_response = requests.get('{url}{year}/{month}/'.format(
                    url=url,
                    year=article_year,
                    month=article_month))

                soup = BeautifulSoup(archive_response.text, 'html.parser')
                archive_list = soup.find(class_='article-list')

                for item in archive_list:
                    article = item.find('a')

                    if article == -1:
                        log.debug('Skipping list item without hyperlink')
                        continue

                    article_link = article['href']

                    if self.args.news_incremental_sync:
                        if self.current_state['articles'].get(article_link):
                            log.debug('%s has already been loaded - skipping!' % article_link)
                            continue

                    article_links.append(article_link)

        if not article_links:
            log.error('Could not find any articles in archive pages!')

        return article_links

    def get_batch_response(self, batch):
        """
        Collect batch of request objects asynchronously
        :return: list
        """
        def exception(request, exception):
            '''
            Exception handler for async requests lib
            '''
            log.error("Unable to process {url}: {error}".format(
                url=request.url,
                error=exception))

        results = grequests.map((grequests.get(u) for u in batch),
                                exception_handler=exception,
                                size=self.args.article_async_threads)

        for article_response in results:
            if not article_response:
                continue
            if article_response.ok:
                yield article_response

    def process_article(self, article_response):
        """
        Parse and process individual articles as Squirro items
        :return: dict
        """
        soup = BeautifulSoup(article_response.text, 'html.parser')
        url = article_response.url
        processed_article = {
            'title': soup.find('title').text,
            'link': url,
            'section': url.split('/')[4].title(),
            'news_source': soup.find("meta", property="og:site_name")['content'],
            'body': soup.find("div", class_="article-content clearfix"),
            'created_at': soup.find("meta", property="article:published_time")['content'][:19],
            'item_type': 'news'
        }

        if self.args.news_incremental_sync:
            self.current_state['articles'][url] = processed_article['title']

        return processed_article

    def getDataBatch(self, batch_size):
        """
        Generator - Get data from source on batches.
        :returns a list of dictionaries
        """
        rows = []

        # This call should ideally `yield` and not return all items directly
        article_count = 0
        for article_batch in self.get_news():
            article_count += len(article_batch)
            log.info('Processing {batch} / {total} articles.'.format(
                batch=article_count,
                total=self.all_articles_count
                ))
            for raw_article in self.get_batch_response(article_batch):
                processed_article = self.process_article(raw_article)
                rows.append(processed_article)
                if len(rows) >= batch_size:
                    yield rows
                    rows = []
        if rows:
            yield rows

    def getSchema(self):
        """
        Return the schema of the dataset
        :returns a List containing the names of the columns retrieved from the
        source
        """
        schema = [
            'title',
            'link',
            'news_source',
            'description',
            'created_at',
            'category',
            'news_section',
            'body'
        ]

        return schema

    def getJobId(self):
        """
        Return a unique string for each different select
        :returns a string
        """
        # Generate a stable id that changes with the main parameters
        m = hashlib.sha256()
        m.update(str(CURRENT_DATETIME))
        job_id = m.hexdigest()
        log.debug("Job ID: %s", job_id)
        return job_id

    def getArguments(self):

        return [
            {
                'name': 'news_incremental_sync',
                'help': 'Toggle incremental loading and state logging for news',
                'default': None,
                'action': 'store_true'
            },
            {
                'name': 'news_state_file',
                'help': 'JSON File for storing previous sync state',
                'default': None,
            },
            {
                'name': 'news_history',
                'help': 'Define number of months back for historic news loading',
                'type': 'int',
                'default': 0
            },
            {
                'name': 'article_async_threads',
                'help': 'Define number parallel requests for scraping',
                'type': 'int',
                'default': 1
            },
            {
                'name': 'article_batch_size',
                'help': 'Define number of full articles held in memory at once',
                'required': True,
                'type': 'int'
            },
            {
                'name': 'load_insurance_journal',
                'help': 'Load articles from Insurance Journal',
                'default': None,
                'action': 'store_true'
            },
            {
                'name': 'load_claims_journal',
                'help': 'Load articles from Claims Journal',
                'default': None,
                'action': 'store_true'
            }
        ]
